![Chocolate](Documentations/Images/DocLogo.png)
-----------------------------------------------

[![Build Status](https://travis-ci.org/Creampuff/chocolate.svg?branch=master)](https://travis-ci.org/Creampuff/chocolate)
[![Linux](https://img.shields.io/badge/os-linux-green.svg?style=flat)](OS)
[![MIT License](http://img.shields.io/badge/license-Apache-blue.svg?style=flat)](LICENSE)

Chocolate is cute, beautiful desktop shell for [Creampuff OS](https://creampuff.github.io).
It provides to users good appearance and fresh experience.
This is designed using CSS like language "qml" and we're also using new programming language, introduced by Apple, named Swift.



#### Features ( Planning )

- Cute and beautiful appearance

- Responsible design

- Using web based technology for basic UI



#### Roadmap
Chocolate develop roadmap is [here](Roadmap/Roadmap.md).


### Cute GUI toolkit Qt
Chocolate shell is adopted Qt as a GUI tool kit.
Qt provides a lot of useful functions such as QtWebEngine, QtSvg and so on.
It also supports web based UI written in HTML and CSS using QtWebEngine or QtWebkit.

- [About Qt](http://www.qt.io/)
- [Qt5.5 Reference](http://doc.qt.io/qt-5/reference-overview.html)

#### Dependences and Includings
 - Gnome Icon Manager
 - Gnome Theme Engine
 - Mutter
 - Polkit
 - Qt Framework

[Read more](Documentations/Dependences.md)


#### Documentations
Documentations are [here](Documentations/Index.md).

#### How to build Chocolate
> Build instrunctions are now under construction.
> Please wait for a moment.

Before you build this application, you have to set up  environment.


##### 1. Set up environment
First, check your operating system. If you use Windows or OSX, you  will be failed to build this application. So, please install Linux operating system such as [Ubuntu](http://www.ubuntu.com), [Linux Mint](http://www.linuxmint.com). Basicaly, you can install any Linux OS but it must be Ubuntu based distribution. For example, Ubuntu Family OS ( [Kubuntu](http://kubuntu.org), [Ubuntu GNOME](https://ubuntugnome.org/), so on ), Linux Mint ( LMDE is Debian based version ) and elementary OS.
You can check your operating system to run following command.
`$uname -a`
If you can't fint litter "Ubuntu", you are not using Ubuntu based OS.

##### 2. Install build dependencies

You have to install requirement to build this applications listed below.


- Build Dependences
	+ Qt Framework
	+ LLVM
	+ Clang
	+ Swift 3.0 ( Development Version )


First, install **Swift and Clang** as follows.
`sudo apt-get install -y clang`
Second, you have to install Swift  compiler for Linux fllowing guide of [Swift.org](https://swift.org) .



#### Open Source
This program is licensed under open source license.
You can use, modify, distribute this program free of charge, see [LICENSE](LICENSE) for detail.
