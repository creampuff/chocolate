Roadmap of Desktop Shell
---------------------

### Overview

- v0.5 Access native ui and gconf

- v0.4 Alternate JavaScript to native api

- v0.3 Create web based shell UI

- v0.2 Port Mutter and Polkit classes to Swift

- v0.1 Port Qt C++ classes to Swift



### Detail

- ##### Ver 0.1
	Port Qt C++ classes such as QtWebkit, QtSvg so that we can use Qt Framework on Swift programming language.
    So we have to develop following modules.
    - Bridging header written in C language
    - Functions wrapper written in Swift


- ##### Ver 0.2
	Port Mutter and Polkit classes to Swift so that we can use these C classes or functions on Swift.
    Mutter is as a window manager that supports OpenGL 3D graphic and Polkit support management of permission.
    We will create these modules to be able to access those modules.
    - Bridging header written in C language.
    - Function wrapper written in Swift

- ##### Ver 0.3
	Create web based shell UI written in CSS, HTML, SVG.
    We uses Sass ( Scss ) to write SCSS efficiently and Jade to efficent management of writing HTML.
    So we have to write AUTO COMPILE SCRIPT to buid these sources that is written in extended-languages.


- ##### Ver 4.0
    Alternate api that is written in JavaScript to native code because JavaScript is slow.
