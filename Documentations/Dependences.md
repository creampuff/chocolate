Dependences
-------------

- Qt5.5
- Polkit
- libmutter
- libwayland-dev
- libwayland-egl1-mesa
- libwayland-server0
- libgles2-mesa-dev
- libxkbcommon-dev


### Build Dependences

- Clang
- LLVM
- Swift 3.0 ( Development Version )
