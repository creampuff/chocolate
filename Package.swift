// Chocolate - A cute, beautiful desktop shell for Creampuff OS.
// This program is mainly written in Swift. ( Current ver is 3.0 development )
// So, this uses Swift Package Manager(SPM).

import PackageDescription


let package = Package(
    name: "Chocolate"
    dependencies: [
        .Package(url: "BridgingHeaders/", majorVersion: 1),
        .Package(url: "3rdparty/SwiftYaml/" majorVersion: 1),
        .Package(url: "Qt/", majorVersion: 1),
        .Package(url: "https://github.com/IBM-Swift/Bridging", majorVersion: 1),
    ],
    exclude: [
        "Artworks",
        "Data/",
        "Documentations/",
        "Roadmap",
    ],
    testDependencies: [
        .Package(url: "Tests/", majorVersion: 1),
    ]
)




// Compiling message.
#if os(Linux)
    print("This platform is supported.")
#else
    FatalError("This platform does not be supported.")
#endif
