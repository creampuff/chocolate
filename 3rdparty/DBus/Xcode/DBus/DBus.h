//
//  DBus.h
//  DBus
//
//  Created by Alsey Coleman Miller on 2/24/16.
//  Copyright © 2016 PureSwift. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for DBus.
FOUNDATION_EXPORT double DBusVersionNumber;

//! Project version string for DBus.
FOUNDATION_EXPORT const unsigned char DBusVersionString[];

