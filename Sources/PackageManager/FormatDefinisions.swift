/**
*   Format Definisions for Chocolate Shell
*   This definision is version 0.1 if you use higher version, you'll be failed to install packages.
*
**/

let FormatVer: Int = 0
let Identification: String = "ChocolateShellExtPkg"
let Requirement = [
      "DataDir": "Data/",
      "InformationDir": "Meta/",
      "Configuration": "Package.yaml"
]
let PackageType = [
      "Theme": "theme",
      "Extension": "ext",
      "Dummy": "dummy",
      "Test": "test"
]
