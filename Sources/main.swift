/*
*    Chocolate Shell - Experimental
*    Copyright (c) 2015-2016 Shota Shimazu
*
*    Licensed under the Apache 2.0, see LICENSE for detail.
*
*/
import Foundation
import Qt




LoadSettings()
LoadExtensions()
if Theme !== Defalut {
    LoadThemeEngine('chocola')
} else {
    LoadThemeEngine(Theme)
}
ExitPanel()
