#!/bin/sh
set -ev

##### Configurations #####
SNAPSHOT_URL=""

bundle exec rake:units
if [ "${TRAVIS_PULL_REQUEST}" = "false" ]; then
	bundle exec rake test:integration
fi



function pltfm_check () {
       if [ -e /etc/lsb-release ]; then
               echo "This platform is not on ci."
               exit
       fi
}



function installation () {
	wget -q -O $SNAPSHOT_URL/$SNAPSHOT
	tar xzf $SNAPSHOT
	export PATH=/opt/swift/usr/bin:"${PATH}"
	echo "Check Installations."
	swift --version
}


function main () {
       pltfm_check
	swift build
}


main ()


