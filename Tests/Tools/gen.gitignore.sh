#!bin/sh
set -e


function gen () {
      touch ../../.gitignore
      cat<<'EOF'>>  ../../.gitignore
# Build Temp
.build/

# Archives
*.zip
*.gz

EOF
       
}



function main () {
        echo ".gitignore generator"
        if [ -e ../.gitignore ]; then
              echo ".gitignore is already exists."
              echo "Do you want to replace current version? ( y/n )"
              read a
              case a in
                    y)
                        rm -rf ../.gitignore
                        gen ()
                    *)
                        echo "Operation have been canceled."
                        echo "Press Enter to exit."
                        read
                        exit
               esac 
         else
               gen ()
         fi
}


main ()
