#!bin/sh 
set -e


function gen () {
      touch ../../.travis.yml
      cat <<'EOF'>> ../../.travis.yml
before_install:
  - sudo apt-get -qq update
  - sudo apt-get install -y clang

script: ./Test/ci.sh

EOF

}


function main () {
        echo ".travis.yml generator"
        if [ -e ../../.travis.yml ]; then
              echo ".travis.yml is already exists."
              echo "Do you want to replace current version? ( y/n )"
              read a
              case a in
                    y)
                        rm -rf ../../.travis.yml
                        gen ()
                    *)
                        echo "Operation have been canceled."
                        echo "Press Enter to exit."
                        read
                        exit
               esac
         else
               gen ()
         fi
}

main ()
